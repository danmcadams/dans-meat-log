<?php

namespace Smokers;

interface MenuItem
{
    public function getName(): string;
}