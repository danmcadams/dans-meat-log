<?php


namespace Smokers;


class NoteImpl implements Note
{
    /**
     * @var string
     */
    protected $content;

    /**
     * NoteImpl constructor.
     *
     * @param string $content
     */
    public function __construct(string $content = '')
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->content;
    }
}