<?php

namespace Smokers;

interface Note
{
    public function getContent(): string;
}