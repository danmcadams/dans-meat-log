<?php

namespace Smokers;

interface Cook
{
    /**
     * @return \DateTime
     */
    public function getEndTime(): ?\DateTime;

    /**
     * @return \DateTime
     */
    public function getStartTime(): ?\DateTime;
}