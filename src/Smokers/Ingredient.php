<?php

namespace Smokers;

interface Ingredient
{
    public function getName(): string;
}