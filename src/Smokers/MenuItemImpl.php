<?php


namespace Smokers;


class MenuItemImpl implements MenuItem
{
    /**
     * @var string
     */
    protected $name;

    /**
     * MenuItemImpl constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}