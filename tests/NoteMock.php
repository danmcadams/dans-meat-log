<?php


namespace Tests;

use Smokers\Note;

class NoteMock implements Note
{
    /**
     * The note itself
     *
     * @return string
     */
    public function getContent(): string
    {
    }
}