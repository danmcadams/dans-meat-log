<?php


namespace Tests;


use PHPUnit\Framework\TestCase;
use Smokers\NoteImpl;

/**
 * Class NoteTest
 *
 * @package Tests
 *
 * @group unit
 * @group note
 */
class NoteTest extends TestCase
{
    public function testCanSetContent()
    {
        $note = new NoteImpl($content = "this is some text that will be a note");

        $this->assertEquals($content, $note->getContent());
        $this->assertEquals($content, (string) $note);
    }
}