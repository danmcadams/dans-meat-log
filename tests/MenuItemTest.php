<?php


namespace Tests;


use PHPUnit\Framework\TestCase;
use Smokers\MenuItemImpl;

/**
 * Class MenuItemTest
 *
 * @package Tests
 *
 * @group unit
 * @group menuItem
 */
class MenuItemTest extends TestCase
{
    public function testCanGetName()
    {
        $itemName = "Dan's Boss Burgers";
        $item     = new MenuItemImpl($itemName);

        $this->assertNotNull($item->getName());
        $this->assertEquals($itemName, $item->getName());
    }
}