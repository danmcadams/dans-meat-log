<?php


namespace Tests;


use PHPUnit\Framework\TestCase;
use Smokers\IngredientImpl;

class IngredientTest extends TestCase
{
    public function testGetIngredientName()
    {
        $ingredient = new IngredientImpl('pepper');
        $ingName    = $ingredient->getName();

        $this->assertEquals('pepper', $ingName);
    }
}