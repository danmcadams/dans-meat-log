<?php


namespace Tests;


use PHPUnit\Framework\TestCase;
use Smokers\Cook;
use Smokers\CookImpl;

/**
 * Class CookTest
 *
 * @package Tests
 *
 * @group unit
 * @group cook
 */
class CookTest extends TestCase
{
    /** @var  Cook|CookImpl */
    private $smokeSession;

    public function setUp()
    {
        $this->smokeSession = new CookImpl();
    }

    public function testNoStartTimeSetReturnNull()
    {
        $this->assertNull($this->smokeSession->getStartTime());
    }

    public function testNoEndTimeSetReturnNull()
    {
        $this->assertNull($this->smokeSession->getStartTime());
    }

    public function testSetStartTimeGetCorrectStartTime()
    {
        $time = new \DateTime('-1 days');

        $this->smokeSession->setStartTime($time);
        $this->assertEquals($time, $this->smokeSession->getStartTime());
    }

    public function testSetEndTimeGetCorrectEndTime()
    {
        $time = new \DateTime('+3 days');

        $this->smokeSession->setEndTime($time);
        $this->assertEquals($time, $this->smokeSession->getEndTime());
    }
}